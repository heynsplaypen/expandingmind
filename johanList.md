# Expanding mind

Collection of books, podcasts and blogs that have formed or inspired my thinking when it comes to science, technology and development.

## Books

* [Black swan](https://en.wikipedia.org/wiki/The_Black_Swan:_The_Impact_of_the_Highly_Improbable). Understanding that most of life is not a nice normal distribution and what is the impact of this on inferring future performance. Important take-away when dealing with non-normal distributions, a new entry can severely skew the prediction (or mean).

 
* [The Phoenix Project](https://www.amazon.com/Phoenix-Project-DevOps-Helping-Business/dp/0988262592), Highlights the basic concepts of devops in the form of a novel. Great way to internalise and understand the abstract theory. Important take-away reduce the work-in-progress (WIP).


## Podcast



## Blogs etc.

* [Hacker laws](https://github.com/dwmkerr/hacker-laws), Comon theories, principles that developers may find useful. 
